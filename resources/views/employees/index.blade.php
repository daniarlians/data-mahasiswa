@extends('layout')

@section('content')

<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#" style="color: #076104;font-size: 35px;font-family: Bernard MT Condensed;">
      <img src="{{ asset('assets/uim.jpg') }}" alt="" width="65" height="45" class="d-inline-block align-text-top">
      Data Mahasiswa Kelas TI-B 2019
    </a>
  </div>
</nav>
  <br>  
<a href="{{route('employee.create')}}" class="btn btn-primary"style="text-align: left;">Tambah Data</a>
<table class="table">

    <tr>
        <th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>No HP</th>
        <th>Tanggal Lahir</th>
        <th>&emsp;&emsp;&ensp;Aksi</th>
    </tr>

    @foreach ($employees as $key => $employee)
    <tr>
        <td>{{ $key+1 }}</td>
        <td>{{$employee->nim}}</td>
        <td>{{$employee->name}}</td>
        <td>{{$employee->designation}}</td>
        <td>{{$employee->number}}</td>
        <td>{{$employee->dob}}</td>
        <td>
            <a href="{{route('employee.edit',$employee->id)}}" class="btn btn-sm btn-outline-primary" >Update</a>
            <form style="display:inline;" action="{{route('employee.destroy',$employee->id)}}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
            </form>
        </td>
    </tr> 
    @endforeach


</table>



@endsection