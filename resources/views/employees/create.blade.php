@extends('layout')

@section('content')
    
<h1 class="mt-5" style="text-align: center;color: #076104;font-family: Bernard MT Condensed;">Tambah Data</h1>

<br>
<form action="{{route('employee.store')}}" method="POST">
    @csrf
    <div class="mb-3">
      <label for="inputNim" class="form-label">NIM </label>
      <input type="number" name='nim' class="form-control" id="inputNim" aria-describedby="emailHelp">
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Nama </label>
      <input type="text" name='name' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="mb-3">
      <label for="exampleInputPassword1" class="form-label">Alamat</label>
      <input type="text" name='designation' class="form-control" id="exampleInputPassword1">
    </div>
    <div class="mb-3">
      <label for="exampleInputPassword1" class="form-label">No HP</label>
      <input type="text" name='number' class="form-control" id="exampleInputPassword1">
    </div>
    <div class="mb-3">
      <label for="exampleInputPassword1" class="form-label">Tanggal Lahir</label>
      <input type="date" name='dob' class="form-control" id="exampleInputPassword1">
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>

@endsection